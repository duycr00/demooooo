var phoneList = [];
var Cartlist = [];
const fetchData = function () {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      phoneList = res.data;
      displayProduct(phoneList);
    })
    .catch(function (err) {
      console.log(err);
    });

  var cartListJSON = localStorage.getItem("cartLists");
  if (cartListJSON) {
    cartList = JSON.parse(cartListJSON);
    cartList = mapData(cartList);
    render();
  }
};

const filter = function () {
  var iphoneList = [];
  var samsungList = [];

  for (i = 0; i < phoneList.length; i++) {
    if (phoneList[i].type === "samsung") {
      samsungList.push(phoneList[i]);
    } else if (phoneList[i].type === "iphone") {
      iphoneList.push(phoneList[i]);
    }
  }
  console.log(phoneList);

  var typeProduct = document.getElementById("select").value;

  if (typeProduct === "1") {
    displayProduct(iphoneList);
    console.log(iphoneList);
  } else if (typeProduct === "2") {
    displayProduct(samsungList);
    console.log(samsungList);
  } else {
    displayProduct(phoneList);
  }
};


const displayProduct = function (data) {
  var content = "";
  for (let i = 0; i < data.length; i++) {
    content += `<div class="col-sm-4">
    <div class ="card" style="width: 18rem;"> 
    <img src="${data[i].img}" alt="">
    <p class="name">${data[i].name}</p>
    <p class="price">Giá Bán: ${data[i].price}</p>
    <p class="backcamera">Camera Sau: ${data[i].backCamera}</p>
    <p class="frontcamera">Camera Trước: ${data[i].frontCamera}</p>
    <p class="rating">Xếp Hạng: ${data[i].rating}</p>
    <button onclick="addToCart('${data[i].id}')" class="btn btn-success">Thêm vào giỏ</button>
    </div>
    </div>`;
  }
  document.querySelector("#displayProduct").innerHTML = content;
};

const findIbdexId = (id)=> {
  
  for (var i = 0; i < phoneList.length; i++) {
    console.log(id, phoneList[i].id);
    if (phoneList[i].id === id) {
      return i;
    }
  }
  return -1;
}

const mapData = function (data) {
  var mappedData = [];
  for (let i = 0; i < data.length; i++) {
    const mappedProduct = new Phone(
      data[i].id,
      data[i].name,
      data[i].img,
      data[i].price,
      data[i].quantity
    );
    mappedData.push(mappedProduct);
  }
  return mappedData;
};

const checkQuantity = function (data, index) {
  if (!data[index].inventory) {
    alert("Sản phẩm đã hết hàng");
    return 1;
  }
};


const addToCart = (id) => {

  var index = findIbdexId(id);
  if (index === -1) {
    alert("Product doesn't exist");
    return;
  };
  checkQuantity(phoneList, index);
  if (checkQuantity(phoneList, index)) {
    return;
  };

  var newcartItem=  new Phone (  phoneList[index].id,
    phoneList[index].name,
    phoneList[index].price,
    phoneList[index].screen,
    phoneList[index].backCamera,
    phoneList[index].frontCamera,
    phoneList[index].img,
    phoneList[index].desc,
    phoneList[index].type,
    1);
    alert("Thêm giỏ hàng thành công");
    
    for (let i = 0; i < Cartlist.length; i++) {
      if (id === Cartlist[i].id) {
        Cartlist[i].quantity += 1;
        phoneList[index].inventory--;
        render();
        return;
      }
    };
  
    Cartlist.push(newcartItem);
    console.log(Cartlist);

   render();
  
  
     

};

///render phonelist


const render = () => {
 
  var contentCart = "";
  for (let i = 0; i < Cartlist.length; i++) {
    contentCart += `<tr>
    <td><img src="${Cartlist[i].img}" style ="height :200px" alt=""></td>
    <td>${Cartlist[i].name}</td>
    <td>${Cartlist[i].price}</td>
    <td>
    ${Cartlist[i].quantity}
    <button onclick="decreaseQuality('${
      Cartlist[i].id
    }')" class="btn btn-danger" id="btnDecrease">-</button>
    <button onclick="increaseQuality('${
      Cartlist[i].id
    }')" class="btn btn-info" id="btnIncrease">+</button>
    </td>
    <td>
    ${Cartlist[i].calcAverage()}
    </td>
    <td><button onclick="deleteFormCart('${
      Cartlist[i].id}')" class="btn btn-danger">Xóa khỏi giỏ hàng</button></td></td>
  </tr>`;
  }
  document.querySelector("#tbodyPhone").innerHTML = contentCart;

 
    caclTotalPrice();
 
};

const findIndexByIdCartItem = function (id) {
  for (i = 0; i < Cartlist.length; i++) {
    if (Cartlist[i].id === id) {
      return i;
    }
  }
  return -1;
};
const decreaseQuality = function (id) {
  var indexCartItem = findIndexByIdCartItem(id);
  var index = findIbdexId(id);
  phoneList[index].inventory++;
  Cartlist[indexCartItem].quantity--;
  if (Cartlist[indexCartItem].quantity === 0) {
    deleteFormCart(id);
    return;
  }
  render();
};
const deleteFormCart = function (id)
{
  var indexIdCartitem = findIndexByIdCartItem(id);
  Cartlist.splice(indexIdCartitem,1);
  render();
  alert("Xóa Thành Công");

}


const increaseQuality = function (id) {
  var indexCartItem = findIndexByIdCartItem(id);
  var index = findIbdexId(id);

  if (checkQuantity(phoneList, index)) {
    return;
  }
  phoneList[index].inventory--;
  Cartlist[indexCartItem].quantity++;
  render();
};
const caclTotalPrice = function ()
{
  var sum = 0;
  for (let i = 0; i < Cartlist.length; i++) {
    sum += Cartlist[i].calcAverage();
  }
  document.getElementById("Sum"
  ).innerHTML = sum;
};

const reset =function ()
{
  var contentCart = "";
  for (let i = 0; i < Cartlist.length; i++) {
    contentCart += `<tr>
    <td></td>
    <td></td>
    <td></td>
    <td>    </td>
    <td>    </td>
    <td></td> 
   
  </tr>`;
  }
  document.querySelector("#tbodyPhone").innerHTML = contentCart;
  alert ("Thanh toán thành công");
  document.getElementById("Sum"
  ).innerHTML = "";

}
fetchData();
